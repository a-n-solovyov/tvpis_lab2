using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;
using System.Linq;


namespace lab2
{
    public static class RegexExtensions
    {
        public static IEnumerable<Match> GetMatches(this Regex source, string data)
        {
            var match = source.Match(data);
            while (match.Success)
            {
                yield return match;
                match = match.NextMatch();
            }
        }
    }

    public static class TokenExtensions
    {
        static readonly string[] Types = Enum.GetNames(typeof(TokenType));

        public static IEnumerable<Token> ProcessTokens(this IEnumerable<Token> source)
        {
            return source.RemoveSpaceBeforeNewlines().RemoveUnnecessaryLines();
        }

        public static IEnumerable<Token> RemoveSpaceBeforeNewlines(this IEnumerable<Token> source)
        {
            using (var enumerator = source.GetEnumerator())
            {
                enumerator.MoveNext();
                Token previous = enumerator.Current;
                Token token = null;
                while (enumerator.MoveNext())
                {
                    token = enumerator.Current;
                    if (token.Type == TokenType.NewLine)
                    {
                        if (previous.Type != TokenType.Whitespaces)
                        {
                            yield return previous;
                        }
                    }
                    else
                    {
                        yield return previous;
                    }

                    previous = token;
                }
            }
        }

        public static IEnumerable<Token> RemoveUnnecessaryLines(this IEnumerable<Token> source)
        {
            Token[] buffer = new Token[3];

            foreach (var token in source)
            {
                if (buffer.Any(x => x?.Type != TokenType.NewLine) || token.Type != TokenType.NewLine)
                {
                    buffer[0] = buffer[1];
                    buffer[1] = buffer[2];
                    buffer[2] = token;
                    yield return token;
                }
            }
        }
        //Test remove


        public static TokenType GetTokenType(Match m)
        {
            foreach (string val in Types)
            {
                if (m.Groups[val].Success)
                {
                    return (TokenType) Enum.Parse(typeof(TokenType), val);
                }
            }

            throw new TokenParseException(@"кривой код или регулярка (нет группы)");
        }
    }


    public enum TokenType
    {
        Identifier,
        Whitespaces,
        NewLine,
        StringLiteral,
        CharLiteral,
        NumberLiteral,
        Operator,
        MultilineComment,
        Punctuation,
        Keyword
    }

    class TokenParseException : Exception
    {
        public TokenParseException(string message) : base(message)
        {
        }

        public TokenParseException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }

    public class Token
    {
        public string Lexeme { get; }

        public TokenType Type { get; }


        public Token(TokenType type, string lexeme)
        {
            Type = type;
            Lexeme = lexeme;
        }

        public Token(Token other)
        {
            Type = other.Type;
            Lexeme = other.Lexeme;
        }
    }

    class Lexer
    {
        private static readonly Regex regex = new Regex(@"
        (?<Identifier>[a-zA-Z_][a-zA-Z_0-9]*) |
        (?<NewLine>\r?\n) |
        (?<StringLiteral>@""([^""]|\n|""{2})*"") |
        (?<CharLiteral>'([^'\\]|\\\\)|\[\'""]') |
        (?<MultilineComment>/\*(.|\n)*?\*/) |
        (?<Operator> \+\+|-- |[+\-*/%] | &&| \|\| | & | \| | \^ | \?\? | [><]=? | [!=]= | << | >> | ([+\-/%*|&\^]| >> | << )?=) |
        (?<NumberLiteral>[0-9]+) |
        (?<Whitespaces>[ \t]+) |
        (?<Punctuation>[{}.();:,\[\]<>]|) |
        (?<SingleLineComment>//.*\n) 
        ", RegexOptions.IgnorePatternWhitespace);

        /*
         *
         * qwer
         *
         *
         */
        public static IEnumerable<Token> GetTokens(string s)
        {
            foreach (var m in regex.GetMatches(s))
            {
                TokenType type = TokenExtensions.GetTokenType(m);

                yield return new Token(type, m.Value);
            }
        }
    }

    internal class Program
    {
        private enum OutputVariant
        {
            Comments,
            Spaces,
            No
        }

        private static readonly OutputVariant Variant = OutputVariant.No;


        public static void Main(string[] args)
        {
            int a = 1;
            a++;
            const string programCs = @"Program.cs";
            DirectoryInfo currentProject = new DirectoryInfo(Directory.GetCurrentDirectory()).Parent.Parent;
            string copyProject = Path.Combine(currentProject.Parent.FullName, @"lab2Copy");


            string programText = File.ReadAllText(Path.Combine(currentProject.FullName, programCs));
            IEnumerable<Token> tokens = Lexer.GetTokens(programText);

            tokens = tokens.ProcessTokens();

            string parsedFilePath = Path.Combine(copyProject, programCs);
            WriteTokensToFile(parsedFilePath, tokens);
        }

        private static void WriteTokensToFile(string filepath, IEnumerable<Token> tokens)
        {
            string output = @"";
            switch (Variant)
            {
                case OutputVariant.Comments:
                    output = string.Join(@"", tokens.Select(x => $@"/*{x.Type}*/{x.Lexeme}"));
                    break;
                case OutputVariant.Spaces:
                    output = string.Join(@" ", tokens.Where(x => x.Type != TokenType.Whitespaces)
                        .Select(x => x.Lexeme)
                    );
                    break;
                case OutputVariant.No:
                    output = string.Join(@"", tokens.Select(x => x.Lexeme));
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            
            File.WriteAllText(filepath, output);
        }
    }
}